variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "mkbehl@gmail.com"
}

variable "db_username" {
  description = "Username for RDS Postgres intance"

}

variable "db_password" {
  description = "Password for RDS Postgre instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}


